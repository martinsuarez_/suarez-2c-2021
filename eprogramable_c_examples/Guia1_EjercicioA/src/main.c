/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t n_led;        //indica el número de led a controlar
	uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t mode;       //ON, OFF, TOGGLE
}leds;

typedef enum{On, Off, Toggle}modo_t;
typedef enum{Led_1=1, Led_2, Led_3}led_t;



/*==================[internal functions declaration]=========================*/
//se declara la función Leds de tipo void que recibe un puntero a una estructura leds
void Leds(leds *led)
{
	uint8_t i=0;
	uint8_t j=0;
	switch(led->mode){
	case On:
		switch(led->n_led){
		case Led_1:
			printf ("Se prende led %u\n",led->n_led);
			break;
		case Led_2:
			printf("Se prende led %u\n",led->n_led);
			break;
		case Led_3:
			printf("Se prende led %u\n",led->n_led);
			break;
		}
		break;
		case Off:
			switch(led->n_led){
			case Led_1:
				printf ("Se apaga led %u\n",led->n_led);
				break;
			case Led_2:
				printf("Se apaga led %u\n",led->n_led);
				break;
			case Led_3:
				printf("Se apaga led %u\n",led->n_led);
				break;
			}
			break;

			case Toggle:
				for(i=0; i<led->n_ciclos; i++){
					switch(led->n_led){
					case Led_1:
						printf("Se togglea Led %u\n",led->n_led);
						break;
					case Led_2:
						printf("Se togglea Led %u\n",led->n_led);
						break;
					case Led_3:
						printf("Se togglea Led %u\n",led->n_led);
						break;
					}
					for(j=0;j<led->periodo;j++){
						printf("Esperando...\r\n");
					}
				}
				break;
	}

}
int main(void)
{
	leds a = {Led_1,4,5,Toggle}; //se declara una variable de tipo leds
	Leds(&a);//se llama a la función Leds


	return 0;
}

/*==================[end of file]============================================*/

