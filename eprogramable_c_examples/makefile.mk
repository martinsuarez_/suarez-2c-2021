########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
##PROYECTO_ACTIVO = 1_hola_mundo
##NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

####Guia1 Ejercicio 1
##PROYECTO_ACTIVO = Guia1_Ejercicio1
##NOMBRE_EJECUTABLE = Guia1_Ejercicio1.exe

####Guia1 Ejercicio 2
##PROYECTO_ACTIVO = Guia1_Ejercicio2
##NOMBRE_EJECUTABLE = Guia1_Ejercicio2.exe

####Guia1 Ejercicio 3
##PROYECTO_ACTIVO = Guia1_Ejercicio3
##NOMBRE_EJECUTABLE = Guia1_Ejercicio3.exe

####Guia1 Ejercicio 7
##PROYECTO_ACTIVO = Guia1_Ejercicio7
##NOMBRE_EJECUTABLE = Guia1_Ejercicio7.exe

####Guia1 Ejercicio 9
##PROYECTO_ACTIVO = Guia1_Ejercicio9
##NOMBRE_EJECUTABLE = Guia1_Ejercicio9.exe

####Guia1 Ejercicio 12
##PROYECTO_ACTIVO = Guia1_Ejercicio12
##NOMBRE_EJECUTABLE = Guia1_Ejercicio12.exe

####Guia1 Ejercicio 14
##PROYECTO_ACTIVO = Guia1_Ejercicio14
##NOMBRE_EJECUTABLE = Guia1_Ejercicio14.exe

####Guia1 Ejercicio 16
##PROYECTO_ACTIVO = Guia1_Ejercicio16
##NOMBRE_EJECUTABLE = Guia1_Ejercicio16.exe

####Guia1 Ejercicio 17
##PROYECTO_ACTIVO = Guia1_Ejercicio17
##NOMBRE_EJECUTABLE = Guia1_Ejercicio17.exe

####Guia1 Ejercicio A
##PROYECTO_ACTIVO = Guia1_EjercicioA
##NOMBRE_EJECUTABLE = Guia1_EjercicioA.exe

####Guia1 Ejercicio C
##PROYECTO_ACTIVO = Guia1_EjercicioC
##NOMBRE_EJECUTABLE = Guia1_EjercicioC.exe

####Guia1 Ejercicio D
PROYECTO_ACTIVO = Guia1_EjercicioD
NOMBRE_EJECUTABLE = Guia1_EjercicioD.exe