/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

//se define la estructura de tipo gpioConf_t
typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;


/*==================[internal functions declaration]=========================*/

//se declara la función BCD_to_port que recibe como parámetro un dígito BCD y un puntero a un arreglo de estructuras del tipo gpioConf_t
void BCD_to_port(uint8_t bcd_digit, gpioConf_t *vec){

	uint8_t mascara=1;
	uint8_t i=0;

	for(i=0;i<4;i++){
		if(bcd_digit&mascara){
			printf("Se pone a 1 puerto %u,pin %u \n",vec[i].port,vec[i].pin);
		}
		else{
			printf("Se pone a 0 puerto %u,pin %u \n",vec[i].port,vec[i].pin);
		}
		//		mascara=1<<i; //no funcionaba con esto, investigar
		mascara=mascara<<1;

	}
}
//se declara la función BinaryToBcd que recibe un entero de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde se almacenarán los n dígitos
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number)
{
	uint32_t aux=data;
	while(digits>0){
		digits--;
		bcd_number[digits]=aux%10;
		aux=aux/10;
	}
}



int main(void)
{
	uint32_t a=1234; //se declara un entero de 32 bits
	uint8_t vect[4]; //se declara un arreglo de enteros de 8 bits
	BinaryToBcd(a,4,vect); //se llama a la función BinaryToBcd

	gpioConf_t vector[4]={{1,4,1},{1,5,1},{1,6,1},{2,14,1}};

	//Se usa la función con 4 dígitos BCD
	printf("Se usa la función BCD_to_port con 4 dígitos BCD\n");
	uint8_t j=0;
	for (j=0;j<4;j++){
		BCD_to_port(vect[j],vector);
	}
	printf("\n");

	//Se usa la función con un único dígito BCD
	printf("Se usa la función BCD_to_port con un único dígito BCD\n");
	BCD_to_port(9,vector);

	return 0;

}
/*==================[end of file]============================================*/

