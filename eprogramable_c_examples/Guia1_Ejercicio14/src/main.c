/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "string.h"

/*==================[macros and definitions]=================================*/
typedef struct {
	uint8_t nombre[12];
	uint8_t apellido[20];
	uint8_t edad;
} alumno; //se define la estructura de tipo alumno

alumno alumno_1; //se declara una variable de tipo alumno
alumno *puntero_a_alumno; //se declara un puntero a alumno

/*==================[internal functions declaration]=========================*/

int main(void)
{
	strcpy(alumno_1.nombre, "Martin"); //se asigna un nombre al alumno
	strcpy(alumno_1.apellido, "Suarez");//se asigna un apellido al alumno
	alumno_1.edad= 21;//se asigna la edad al alumno

	puntero_a_alumno = &alumno_1; //se asigna al puntero la posición de alumno_1

	printf("Nombre: %s \n",puntero_a_alumno->nombre);
	printf("Apellido: %s \n",puntero_a_alumno->apellido);
	printf("Edad: %u \n",puntero_a_alumno->edad);

	strcpy(puntero_a_alumno->nombre, "Maria"); //se cambia el nombre al alumno, accediendo a este con el puntero mediante el operador de acceso
	strcpy(puntero_a_alumno->apellido,"Casablanca");//se cambia el apellido al alumno, accediendo a este con el puntero mediante el operador de acceso
	puntero_a_alumno->edad=21;//se cambia la edad al alumno, accediendo a este con el puntero mediante el operador de acceso

	printf("Nombre: %s \n",puntero_a_alumno->nombre);
	printf("Apellido: %s \n",puntero_a_alumno->apellido);
	printf("Edad: %u \n",puntero_a_alumno->edad);

return 0;
}

/*==================[end of file]============================================*/

