/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
/*==================[macros and definitions]=================================*/
#define BIT_8 8
#define BIT_16 16
#define BIT_24 24


uint32_t variable= 0x01020304; //se declara una variable de 32 bits y se le asigna el valor 0x01020304
//se declaran 4 variables de 8 bits para cargar los 4 bytes de la variable de 32
uint8_t variable_1;
uint8_t variable_2;
uint8_t variable_3;
uint8_t variable_4;
//se declara una variable de 32 bits auxiliar
uint32_t auxiliar;

union test{
	struct{
		uint8_t byte_1;
		uint8_t byte_2;
		uint8_t byte_3;
		uint8_t byte_4;
	}cada_byte;
	uint32_t  todos_los_bytes;
};//se define una unión de tipo test


/*==================[internal functions declaration]=========================*/

int main(void)
{
	variable_1=(uint8_t) variable;//se cargan los 8 bits menos significativos de variable en variable_1
	auxiliar=variable>>BIT_8;  //se asigna a auxiliar variable con sus bits desplazados 8 lugares a la derecha
	variable_2=(uint8_t)auxiliar ;//se cargan desde el bit 8 a 15 de variable en variable_2
	auxiliar=variable>>BIT_16;//se asigna a auxiliar variable con sus bits desplazados 16 lugares a la derecha
	variable_3=(uint8_t) auxiliar; //se cargan desde el bit 16 a 23 de variable en variable_3
	auxiliar=variable>>BIT_24;//se asigna a auxiliar variable con sus bits desplazados 24 lugares a la derecha
	variable_4=(uint8_t) auxiliar;//se cargan desde el bit 24 a 32 de variable en variable_4

	printf("Variable_1= %u \n",variable_1);
	printf("Variable_2= %u \n",variable_2);
	printf("Variable_3= %u \n",variable_3);
	printf("Variable_4= %u \n",variable_4);

	union test variable_union; //se declara una unión de tipo test

	variable_union.todos_los_bytes=variable; //se asigna a variable_union el valor de variable

	//se muestran los 4 bytes de variable_unión accediendo a estos por separado mediante el operador de acceso
	printf("Byte1= %u \n",variable_union.cada_byte.byte_1);
	printf("Byte2= %u \n",variable_union.cada_byte.byte_2);
	printf("Byte3= %u \n",variable_union.cada_byte.byte_3);
	printf("Byte4= %u \n",variable_union.cada_byte.byte_4);

	return 0;
}

/*==================[end of file]============================================*/

