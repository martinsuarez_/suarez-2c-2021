/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
//se declara la función BinaryToBcd que recibe un entero de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde se almacenarán los n dígitos
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number)
{
	uint32_t aux=data;
	while(digits>0){
		digits--;
		bcd_number[digits]=aux%10;
		aux=aux/10;
		printf("digito %u: %u\n", digits+1, bcd_number[digits]);
	}
}

int main(void)
{uint32_t a=15648; //se declara un entero de 32 bits
uint8_t vector[5]; //se declara un arreglo de enteros de 8 bits
BinaryToBcd(a,5,vector); //se llama a la función BinaryToBcd

uint8_t i=0;
for(i=0;i<5;i++){
	printf("%u", vector[i]);
}

return 0;
}

/*==================[end of file]============================================*/

