/* Copyright 2021,
 * Martin Suarez
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef GONIOMETRO_H
#define GONIOMETRO_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup GONIOMETRO Goniometro
 ** @{ */

/** @brief Bare Metal header for Goniometro on EDU-CIAA NXP
 **
 **
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	MS			Martin Suarez
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211101 v0.1 initials initial version
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "analog_io.h"

/*==================[macros]=================================================*/


/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/




/*==================[external functions declaration]=========================*/

/** @fn bool GoniometroInit(uint8_t midpoint)
 * @brief Initialization function of Goniometro.
 * @param[in] Analog Channel connected to the midpoint output pin on the device (CH0, CH1, CH2, CH3)
 * @return TRUE if no error.
 */
bool GoniometroInit(uint8_t midpoint);

/** @fn uint8_t GoniometroMeasureAngle(void)
 * @brief Measures the current angle
 * @param[in] No Parameter
 * @return value of angle in degrees
 */
uint8_t GoniometroMeasureAngle(void);
/*==================[end of file]============================================*/
#endif /* #ifndef GONIOMETRO_H */

