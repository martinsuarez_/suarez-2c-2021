/* Copyright 2021,
 * Maria Casablanca
 * mariacasabla@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	MC         Maria Casablanca
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210901 v0.1 initials initial version Maria Casablanca
 */

/*==================[inclusions]=============================================*/
#include "../../drivers_devices/inc/Si7007.h"

#include "gpio.h"
#include "delay.h"
#include "chip.h"


/*==================[macros and definitions]=================================*/

#define POWER_SUPPLY 3.3         /**< Puerto del hardware*/
#define TOTAL_BITS 1024          /**< Puerto del hardware*/

/*==================[internal data declaration]==============================*/

analog_input_config temp_config;
analog_input_config hum_config;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool Si7007Init(Si7007_config *pins){


	/*GPIOInit(pins->select, GPIO_OUTPUT);
	GPIOOff(pins->select); //Lo pongo en 0 para que PWM 1 sea temperatura y PWM 2 humedad.

	temp_config.input = pins->PWM_1;;
	temp_config.mode = AINPUTS_SINGLE_READ;
	temp_config.pAnalogInput = NULL;

	hum_config.input = pins->PWM_2;
	hum_config.mode = AINPUTS_SINGLE_READ;
	hum_config.pAnalogInput = NULL;*/

	GPIOInit(pins->select, GPIO_OUTPUT);
	GPIOOn(pins->select); //Lo pongo en 0 para que PWM 1 sea temperatura y PWM 2 humedad.

	temp_config.input = pins->PWM_2;;
	temp_config.mode = AINPUTS_SINGLE_READ;
	temp_config.pAnalogInput = NULL;

	hum_config.input = pins->PWM_1;
	hum_config.mode = AINPUTS_SINGLE_READ;
	hum_config.pAnalogInput = NULL;



}
float Si7007MeasureTemperature(void){

	uint16_t value;
	float conversion = 0;
	float temperature = 0;
	float valor = 0;
	AnalogInputInit(&temp_config);
	AnalogInputReadPolling(temp_config.input, &value);
	conversion = value*POWER_SUPPLY/TOTAL_BITS;
	valor = conversion/POWER_SUPPLY;
	temperature = -46.85 + (valor*(175.71));
	return temperature;

}

float Si7007MeasureHumidity(void){

	uint16_t value;
	float conversion = 0;
	float humidity = 0;
	float valor = 0;
	AnalogInputInit(&hum_config);
	AnalogInputReadPolling(hum_config.input, &value);
	conversion = value*POWER_SUPPLY/TOTAL_BITS;
	valor = conversion/POWER_SUPPLY;
	humidity = -6 + (valor*125);
	return humidity;
}

bool Si7007Deinit(gpio_t select, uint8_t PWM_1, uint8_t PWM_2){



}


/*==================[end of file]============================================*/
