/* Copyright 2021,
 * Maria Casablanca
 * mariacasabla@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	MC         Maria Casablanca
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210901 v0.1 initials initial version Maria Casablanca
 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/

#define CONSTANTE_CENTIMETROS 30   /*!< Constante por la cual se divide el pulso (en uS) para obtener la distancia en cm */
#define CONSTANTE_PULGADAS 148     /*!< Constante por la cual se divide el pulso (en uS) para obtener la distancia en pulgadas  */
#define TIEMPO_TRIGGER 10          /*!< Tiempo del pulso para trigger  */


/*==================[internal data declaration]==============================*/

gpio_t echo_global;
gpio_t trigger_global;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger){

	/** Configuration of the GPIO */

	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);

	GPIOOff(trigger);

	echo_global = echo;
	trigger_global = trigger;

	return true;
}


uint16_t HcSr04ReadDistanceCentimeters(void){

	uint16_t tiempo = 0;
	uint16_t distancia_centimetros = 0;
	GPIOOn(trigger_global);
	DelayUs(TIEMPO_TRIGGER);
	GPIOOff(trigger_global);

	while(!GPIORead(echo_global)){


	}

	while(GPIORead(echo_global)){
		DelayUs(1);
		tiempo ++;
	}

	distancia_centimetros = tiempo/CONSTANTE_CENTIMETROS;
	return distancia_centimetros;

}

uint16_t HcSr04ReadDistanceInches(void){

	uint16_t tiempo = 0;
	uint16_t distancia_pulgadas = 0;

	GPIOOn(trigger_global);
	DelayUs(TIEMPO_TRIGGER);
	GPIOOff(trigger_global);

	while(!GPIORead(echo_global)){


	}

	while(GPIORead(echo_global)){
		DelayUs(1);
		tiempo ++;
	}

	distancia_pulgadas = tiempo/CONSTANTE_PULGADAS;

	return distancia_pulgadas;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger){


	GPIODeinit();

	return true;
}



/*==================[end of file]============================================*/
