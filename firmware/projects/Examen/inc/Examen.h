/*! @mainpage Examen
 *
 * \section genDesc General Description
 *
 * This application works as a sonar. It senses near objects every 15 degrees.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Potenciometer	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC  		| 	VDDA		|
 * | 	Mid Point	| 	CH1		    |
 * | 	GND			| 	GNDA		|
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Martin Suarez
 *
 */

#ifndef _EXAMEN_H
#define _EXAMEN_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EXAMEN_H */

