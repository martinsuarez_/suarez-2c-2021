/*! @mainpage Examen
 *
 * \section genDesc General Description
 * This application works as a sonar. It senses near objects every 15 degrees.
 *
 * \section hardConn Hardware Connection
 *
 * | Potenciometro	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC  		| 	VDDA		|
 * | 	Mid Point	| 	CH1		    |
 * | 	GND			| 	GNDA		|
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Martin Suarez
 *
 */

/*==================[inclusions]=============================================*/
#include "Examen.h"       /* <= own header */
#include "goniometro.h"
#include "hc_sr4.h"
#include "led.h"
#include "uart.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
uint8_t angle;
uint8_t object_distance;
uint8_t nearest_object_angle;
uint8_t nearest_object_distance;

/*==================[internal functions declaration]=========================*/

/** @fn void Measure(void)
 * @brief Function to Measure object distance every 15 degrees
 * @param[in] no parameter
 * @return TRUE if no error
 */
void Measure(void);

/** @fn void SisInit(void)
 * @brief
 * @param[in] no parameter
 * @return TRUE if no error
 */
void SisInit(void);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,1000,Measure};					/* !<struct for timer configuration*/
serial_config UART_USB = {SERIAL_PORT_PC,115200,NULL};			/* !<struct for serial port configuration*/

/*==================[external functions definition]==========================*/

void Measure(void){

	angle=GoniometroMeasureAngle();


	switch(angle){
	case 0:
		object_distance=HcSr04ReadDistanceCentimeters();
		UartSendString(SERIAL_PORT_PC, UartItoa(angle, 10));
		UartSendString(SERIAL_PORT_PC, "° objeto a ");
		UartSendString(SERIAL_PORT_PC, UartItoa(object_distance, 10));
		UartSendString(SERIAL_PORT_PC, " cm \r\n");

		nearest_object_angle=angle;
		nearest_object_distance=object_distance;

		LedOff(LED_RGB_G);
		LedOn(LED_RGB_R);


		break;
	case 15:
		object_distance=HcSr04ReadDistanceCentimeters();
		UartSendString(SERIAL_PORT_PC, UartItoa(angle, 10));
		UartSendString(SERIAL_PORT_PC, "° objeto a ");
		UartSendString(SERIAL_PORT_PC, UartItoa(object_distance, 10));
		UartSendString(SERIAL_PORT_PC, " cm \r\n");

		if(object_distance<nearest_object_distance){
			nearest_object_angle=angle;
			nearest_object_distance=object_distance;
		}
		break;
	case 30:
		object_distance=HcSr04ReadDistanceCentimeters();
		UartSendString(SERIAL_PORT_PC, UartItoa(angle, 10));
		UartSendString(SERIAL_PORT_PC, "° objeto a ");
		UartSendString(SERIAL_PORT_PC, UartItoa(object_distance, 10));
		UartSendString(SERIAL_PORT_PC, " cm \r\n");

		if(object_distance<nearest_object_distance){
			nearest_object_angle=angle;
			nearest_object_distance=object_distance;
		}
		break;
	case 45:
		object_distance=HcSr04ReadDistanceCentimeters();
		UartSendString(SERIAL_PORT_PC, UartItoa(angle, 10));
		UartSendString(SERIAL_PORT_PC, "° objeto a ");
		UartSendString(SERIAL_PORT_PC, UartItoa(object_distance, 10));
		UartSendString(SERIAL_PORT_PC, " cm \r\n");

		if(object_distance<nearest_object_distance){
			nearest_object_angle=angle;
			nearest_object_distance=object_distance;
		}
		break;
	case 60:
		object_distance=HcSr04ReadDistanceCentimeters();
		UartSendString(SERIAL_PORT_PC, UartItoa(angle, 10));
		UartSendString(SERIAL_PORT_PC, "° objeto a ");
		UartSendString(SERIAL_PORT_PC, UartItoa(object_distance, 10));
		UartSendString(SERIAL_PORT_PC, " cm \r\n");

		if(object_distance<nearest_object_distance){
			nearest_object_angle=angle;
			nearest_object_distance=object_distance;
		}
		break;
	case 75:
		object_distance=HcSr04ReadDistanceCentimeters();
		UartSendString(SERIAL_PORT_PC, UartItoa(angle, 10));
		UartSendString(SERIAL_PORT_PC, "° objeto a ");
		UartSendString(SERIAL_PORT_PC, UartItoa(object_distance, 10));
		UartSendString(SERIAL_PORT_PC, " cm \r\n");

		if(object_distance<nearest_object_distance){
			nearest_object_angle=angle;
			nearest_object_distance=object_distance;
		}
		UartSendString(SERIAL_PORT_PC, "Obstaculo en ");
		UartSendString(SERIAL_PORT_PC, UartItoa(nearest_object_angle, 10));
		UartSendString(SERIAL_PORT_PC, "° \r\n");



		LedOff(LED_RGB_R);
		LedOn(LED_RGB_G);
		break;
	}

}


void SisInit(void){

	GoniometroInit(CH1);
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	LedsInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);

}
int main(void){

	while(1){

	}

	return 0;
}

/*==================[end of file]============================================*/

