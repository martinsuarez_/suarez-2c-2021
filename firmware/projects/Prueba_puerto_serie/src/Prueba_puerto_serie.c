/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Prueba_puerto_serie.h"       /* <= own header */
//#include "sapi_rtc.h"
//#include "Si7007.h"
//#include "switch.h"
//#include "timer.h"
#include "gpio.h"
#include "uart.h"
//#include "pwm_sct.h"

/*==================[macros and definitions]=================================*/
#define LOW            0
#define HIGH           1

/*==================[internal data definition]===============================*/

bool blinds_status;

/*==================[internal functions declaration]=========================*/
void VariablesMonitoring(void);
void ReturnBlindStatus(void);
//void ReturnMeasurements(void);

/*==================[external data definition]===============================*/
serial_config UART_USB = {SERIAL_PORT_PC,115200,VariablesMonitoring};

/*==================[external functions definition]==========================*/

void VariablesMonitoring(void){
	//bool control_m=FALSE;
	uint8_t key;

	UartReadByte(SERIAL_PORT_PC, &key);

	switch (key){
	case 'p':
		ReturnBlindStatus();
		break;
	case 'm':
		//control_m=TRUE;
		UartSendString(SERIAL_PORT_PC, "ingrese 'h' para ver % de humedad, 't' para temperatura e 'i' para iluminacion \r\n");
		//		UART_USB.pSerial=ReturnMeasurements;
		break;
	case 'h':
		//if(control_m)
		UartSendString(SERIAL_PORT_PC, "Humedad:\r\n ");
		//UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureHumidity(), 10));
		//UartSendString(SERIAL_PORT_PC, " % \r\n");

		break;
	case 't':
		//	if(control_m)
		UartSendString(SERIAL_PORT_PC, "Temperatura:\r\n ");
		//UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureTemperature(), 10));
		//UartSendString(SERIAL_PORT_PC, " °C \r\n");
		break;

	case 'i':
		//		if(control_m)
		UartSendString(SERIAL_PORT_PC, "Iluminacion:\r\n ");
		//falta devolver iluminacion a partir del LDR
		//UartSendString(SERIAL_PORT_PC, " lux \r\n");
		break;

		//		ReturnMeasurements();
		//break;
	}
}


void ReturnBlindStatus(void){

	switch (blinds_status){
	case LOW:
		UartSendString(SERIAL_PORT_PC, "Las persianas se encuentran en bajo \r\n");
		break;
	case HIGH:
		UartSendString(SERIAL_PORT_PC, "Las persianas se encuentran en alto \r\n");
		break;
	}
}

//void ReturnMeasurements(void){
//
//	uint8_t key;
//
//	UartSendString(SERIAL_PORT_PC, "ingrese 'h' para ver % de humedad, 't' para temperatura e 'i' para iluminacion \r\n");
//
//	UartReadByte(SERIAL_PORT_PC, &key);
//	switch (key){
//	case 'h':
//		UartSendString(SERIAL_PORT_PC, "Humedad: ");
//		//UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureHumidity(), 10));
//		//UartSendString(SERIAL_PORT_PC, " % \r\n");
//
//		break;
//	case 't':
//		UartSendString(SERIAL_PORT_PC, "Temperatura: ");
//		//UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureTemperature(), 10));
//		//UartSendString(SERIAL_PORT_PC, " °C \r\n");
//		break;
//	case 'i':
//		UartSendString(SERIAL_PORT_PC, "Iluminacion: ");
//		//falta devolver iluminacion a partir del LDR
//		//UartSendString(SERIAL_PORT_PC, " lux \r\n");
//		break;
//	}
//	//UART_USB.pSerial=VariablesMonitoring;
//
//}

int main(void){
	UartInit(&UART_USB);
	blinds_status=LOW;


	while(1){

	}

	return 0;
}

/*==================[end of file]============================================*/

