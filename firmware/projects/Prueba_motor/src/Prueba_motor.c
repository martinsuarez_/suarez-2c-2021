/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | 	Motor 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 VDD  	    |  	  3,3 V	    |
 * | 	 PWM1	    | 	  TFIL2		|
 * | 	 PWM2	    | 	  TFIL1   	|
 * |   	 DO1        |  	  GPIO3		|
 * |   	 DO2  	    |  	  GPIO1		|
 * |   	 GND 	    |  	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Prueba_motor.h"       /* <= own header */
//#include "sapi_rtc.h"
//#include "Si7007.h"
//#include "switch.h"
//#include "timer.h"
#include "gpio.h"
#include "uart.h"
#include "pwm_sct.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
pwm_out_t ctout[]={CTOUT0 , CTOUT1};
bool sentido;

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
void Arriba(void){
sentido = TRUE;
}

void Abajo(void){
sentido = FALSE;
}
int main(void){

	GPIOInit(GPIO_1, GPIO_INPUT);
	GPIOInit(GPIO_3, GPIO_INPUT);
	PWMInit(ctout, 2, 20000);

	GPIOActivInt(GPIOGP0,GPIO_1, Arriba, FALSE);
	GPIOActivInt(GPIOGP1,GPIO_3, Abajo, FALSE);

	PWMSetDutyCycle(CTOUT0, 0);
	PWMSetDutyCycle(CTOUT1, 70);
	PWMOn();



		while(1){
//			PWMSetDutyCycle(CTOUT0, 85);//(mueve) //que dutycycle poner?
//			PWMSetDutyCycle(CTOUT1, 0);
//			PWMOn();
//			while(GPIORead(GPIO_3)==FALSE){ //mientras no llegue al final de carrera
//
//			}
//			PWMOff(); //paro el motor
//			//PWMSetDutyCycle(CTOUT0, 0); //con esto paro el motor?
//			//blinds_status=HIGH;
//
//			PWMSetDutyCycle(CTOUT1, 85); //(mueve) // //que dutycycle poner?
//			PWMSetDutyCycle(CTOUT0, 0);
//			PWMOn();
//			while(GPIORead(GPIO_1)==FALSE){ //mientras no llegue al final de carrera
//
//			}
//			PWMOff(); //paro el motor
//			//PWMSetDutyCycle(CTOUT1, 0); //con esto paro el motor?
//			//blinds_status=LOW;

			if(sentido){
			    PWMSetDutyCycle(CTOUT0, 0);
			    PWMSetDutyCycle(CTOUT1,70);

			    }else{
			    PWMSetDutyCycle(CTOUT1, 0);
			    PWMSetDutyCycle(CTOUT0, 70);
			    }

		}
//		PWMOff();
	return 0;
}

/*==================[end of file]============================================*/

