/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/prueba_LCD.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

	gpio_t pins_config[7]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};
/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){



	ITSE0803Init(pins_config);
	ITSE0803DisplayValue(150);
	uint8_t auxiliar=ITSE0803ReadValue();

    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

