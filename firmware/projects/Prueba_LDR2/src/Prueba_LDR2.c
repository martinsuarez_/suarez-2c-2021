/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Prueba_LDR2.h"       /* <= own header */
#include "gpio.h"
#include "uart.h"

//#include "analog_io.h"
/*==================[macros and definitions]=================================*/
#define LOW_ILUMINATION            1
#define HIGH_ILUMINATION           0

/*==================[internal data definition]===============================*/
//analog_input_config ad_config{CH3,AINPUTS_SINGLE_READ,NULL};

/*==================[internal functions declaration]=========================*/
void ControlVentanas(void);

/*==================[external data definition]===============================*/
serial_config UART_USB = {SERIAL_PORT_PC,115200,ControlVentanas};
bool ilumination;

/*==================[external functions definition]==========================*/

void ControlVentanas(void){

	uint8_t key;

	UartReadByte(SERIAL_PORT_PC, &key);

	if(key=='i'){
		//ilumination=GPIORead(GPIOGP7);
			if(ilumination==LOW_ILUMINATION){
				UartSendString(SERIAL_PORT_PC, "Iluminacion baja:levantar persianas \r\n");
			}
			if(ilumination==HIGH_ILUMINATION){
				UartSendString(SERIAL_PORT_PC, "Iluminacion alta:bajar persianas \r\n");
			}
//		switch (ilumination){
//		case LOW:
//			UartSendString(SERIAL_PORT_PC, "Iluminacion baja:levantar persianas \r\n");
//			break;
//		case HIGH:
//			UartSendString(SERIAL_PORT_PC, "Iluminacion alta:bajar persianas \r\n");
//			break;
//		}
	}
//
//	if(GPIORead(GPIOGP7)==FALSE){
//
//	}
//	if(GPIORead(GPIOGP7)==TRUE){
//
//	}

}

int main(void){
	//gpio_t GPIO_7=17;

	UartInit(&UART_USB);
	GPIOInit(GPIO_LCD_1,GPIO_INPUT);


	while(1){

		ilumination=GPIORead(GPIO_LCD_1);
	}

	return 0;
}

/*==================[end of file]============================================*/

