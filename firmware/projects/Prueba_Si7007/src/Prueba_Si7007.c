/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Prueba_Si7007.h"       /* <= own header */
#include "Si7007.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void ReturnMeasurements(void);

/*==================[external data definition]===============================*/
serial_config UART_USB = {SERIAL_PORT_PC,115200,ReturnMeasurements};

/*==================[external functions definition]==========================*/
void ReturnMeasurements(void){

	uint8_t key;



		UartReadByte(SERIAL_PORT_PC, &key);
		switch (key){
		case 'h':
			UartSendString(SERIAL_PORT_PC, "Humedad: ");
			UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureHumidity(), 10));
			UartSendString(SERIAL_PORT_PC, " %\r\n");

			break;
		case 't':
			UartSendString(SERIAL_PORT_PC, "Temperatura: ");
			UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureTemperature(), 10));
			UartSendString(SERIAL_PORT_PC, " °C\r\n");
			break;
//		case 'i':
//			UartSendString(SERIAL_PORT_PC, "Iluminacion: ");
//			//falta devolver iluminacion a partir del LDR
//			UartSendString(SERIAL_PORT_PC, " lux\r\n");
//			break;
		}
}

int main(void){
	UartInit(&UART_USB);
	Si7007Init(GPIO_5,CH1,CH2);

	UartSendString(SERIAL_PORT_PC, "ingrese 'h' para ver % de humedad o 't' para ver temperatura\r\n");



    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

