/*! @mainpage Proyecto_Ventanas_CIEV
 *
 * \section genDesc General Description
 *
 * This application controls blinds manually and automatically.
 *
 * \section hardConn Hardware Connection
 *
 * |   Si7007	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	SELECT	 	| 	 GPIO5	    |
 * | 	VDD	 		| 	 VDDA		|
 * | 	PWM2 	 	| 	 CH1    	|
 * | 	PWM1	  	| 	 CH2		|
 * |	GND			|	 GNDA 		|
 *
 * | 	Motor 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 VDD  	    |  	  5 V	    |
 * | 	 PWM1	    | 	  TFIL2		|
 * | 	 PWM2	    | 	  TFIL1   	|
 * |   	 DO1        |  	  GPIO3		|
 * |   	 DO2  	    |  	  GPIO1		|
 * |   	 GND 	    |  	  GND		|
 *
 * | Light sensor	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VDD	 		| 	 3,3 V	    |
 * | 	GND	 		| 	 GND		|
 * | 	DO 	 		| 	 LCD1		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Martin Suarez
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto_Ventanas_CIEV/inc/Proyecto_Ventanas_CIEV.h"       /* <= own header */
#include "sapi_rtc.h"
#include "Si7007.h"
#include "switch.h"
#include "timer.h"
//#include "gpio.h"
#include "uart.h"
#include "pwm_sct.h"
#include "led.h"

/*==================[macros and definitions]=================================*/

#define LOW            				0
#define HIGH           				1
#define UP                          1
#define DOWN                        0
#define SUNDAY                      1
#define SATURDAY                    7
#define MAX_TEMPERATURE             30

typedef enum  {hora, minutos, dia_mes, dia_semana, mes, anio}time_t;

/*==================[internal data definition]===============================*/


bool blinds_status; 														/* !<controls blinds status*/
bool manual_control;														/* !<controls system control status*/
bool set_clock = FALSE;
time_t time_var = hora;
bool clock_ready=FALSE;														/* !<checks if date is already configured*/
bool direction;                            									/* !<controls spin direction of blinds motor*/

/*==================[internal functions declaration]=========================*/

/** @fn void AutoControlBlinds(void)
 * @brief Function to automatic control blinds
 * @param[in] no parameter
 * @return TRUE if no error
 */
void AutoControlBlinds(void);

/** @fn void ChangeControlMethod(void)
 * @brief Function to change control method (manual to automatic - automatic to manual)
 * @param[in] no parameter
 * @return TRUE if no error
 */
void ChangeControlMethod(void);

/** @fn void RaiseBlinds(void)
 * @brief Function to raise blinds
 * @param[in] no parameter
 * @return TRUE if no error
 */
void RaiseBlinds(void);

/** @fn void LowerBlinds(void)
 * @brief Function to lower blinds
 * @param[in] no parameter
 * @return TRUE if no error
 */
void LowerBlinds(void);

/** @fn void ManualLowerBlinds(void)
 * @brief Function to lower blinds if manual_cont==TRUE
 * @param[in] no parameter
 * @return TRUE if no error
 */
void ManualLowerBlinds(void);

/** @fn void ManualRaiseBlinds(void)
 * @brief Function to raise blinds if manual_cont==TRUE
 * @param[in] no parameter
 * @return TRUE if no error
 */
void ManualRaiseBlinds(void);

/** @fn void ReturnBlindStatus(void)
 * @brief Function to show blinds status
 * @param[in] no parameter
 * @return TRUE if no error
 */
void ReturnBlindStatus(void);

/** @fn bool MeasureIlumination(void)
 * @brief Function to measure ilumination
 * @param[in] no parameter
 * @return HIGH if high ilumination/ LOW if low ilumination
 */
bool MeasureIlumination(void);

/** @fn void Menu(void)
 * @brief Function to guide the program user
 * @param[in] no parameter
 * @return TRUE if no error
 */
void Menu(void);

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,1000,&AutoControlBlinds};		/* !<struct for timer configuration*/
serial_config UART_USB = {SERIAL_PORT_PC,115200,&Menu};			/* !<struct for serial port configuration*/
rtc_t hora_config;												/* !<rtc_t struct for date configuration*/
pwm_out_t ctout[]={CTOUT0 , CTOUT1};							/* !<PWM inputs for blinds motor*/
Si7007_config HT_config={GPIO_5,CH1,CH2};						/* !<struct for Si7007 configuration*/


/*==================[external functions definition]==========================*/


void AutoControlBlinds(void){

	if (manual_control==TRUE){

	}
	if (manual_control==FALSE){
		rtcRead(&hora_config);
		if (blinds_status==HIGH){

			if (SUNDAY<hora_config.wday&&hora_config.wday<SATURDAY){

				if(8<hora_config.hour&&hora_config.hour<18){

					if(Si7007MeasureTemperature()<MAX_TEMPERATURE){
						if(MeasureIlumination()==LOW){
							//no hacer nada, quedan en alto
						}
						else{
							LowerBlinds();

						}

						//no hacer nada, quedan en alto
					}
					else{
						LowerBlinds();

					}

				}
				else{
					LowerBlinds();

				}
			}
			else{
				LowerBlinds();

			}

		}
		else //significa que están bajas
		{

			if (SUNDAY<hora_config.wday&&hora_config.wday<SATURDAY){

				if(8<hora_config.hour&&hora_config.hour<18){

					if(Si7007MeasureTemperature()<MAX_TEMPERATURE){

						if(MeasureIlumination()==LOW){

							RaiseBlinds();

						}
						else{
							//no hacer nada, quedan en bajo
						}

					}

					else{
						//no hacer nada, quedan en bajo
					}
				}

				else{
					//no hacer nada, quedan en bajo
				}
			}

			else{
				//no hacer nada, quedan en bajo
			}
		}

	}
}

void ChangeControlMethod(void){

	if(clock_ready){

		manual_control=!manual_control;

		if(manual_control==TRUE){
			UartSendString(SERIAL_PORT_PC, "Control manual activado\r\n");
			UartSendString(SERIAL_PORT_PC, "Presione TEC1 para levantar las persianas o TEC2 para bajarlas\r\n");
		}
		if(manual_control==FALSE)
			UartSendString(SERIAL_PORT_PC, "Control automatico activado\r\n");
	}
	else{
		UartSendString(SERIAL_PORT_PC, "Debe cargar la fecha y hora previamente, presione d\r\n");
	}
}

void RaiseBlinds(void){

	PWMSetDutyCycle(CTOUT0,65);
	PWMSetDutyCycle(CTOUT1, 0);
	PWMOn();
	while(GPIORead(GPIO_3)==TRUE){ 			//mientras no llegue al final de carrera

	}
	PWMSetDutyCycle(CTOUT0, 0);
	PWMOff();



	blinds_status=HIGH;

}


void LowerBlinds(void){

	PWMSetDutyCycle(CTOUT1, 65);
	PWMSetDutyCycle(CTOUT0, 0);
	PWMOn();
	while(GPIORead(GPIO_1)==TRUE){ 			//mientras no llegue al final de carrera

	}
	PWMSetDutyCycle(CTOUT1, 0);
	PWMOff();


	blinds_status=LOW;

}

void ManualRaiseBlinds(void){
	if(manual_control==TRUE){
		if(blinds_status==LOW){
			RaiseBlinds();
		}
	}
}
void ManualLowerBlinds(void){
	if(manual_control==TRUE){
		if(blinds_status==HIGH){
			LowerBlinds();
		}
	}
}

void ReturnBlindStatus(void){

	switch (blinds_status){
	case LOW:
		UartSendString(SERIAL_PORT_PC, "Las persianas se encuentran en bajo\r\n");
		break;
	case HIGH:
		UartSendString(SERIAL_PORT_PC, "Las persianas se encuentran en alto\r\n");
		break;
	}
}

bool MeasureIlumination(void){
	bool ilumination=!GPIORead(GPIO_LCD_1);

	return ilumination;
}

void Menu(void){
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);
	/******* RECUPERO VALORES NUMERICOS **********/

	if(set_clock){
		switch(time_var){
		case hora:
			hora_config.hour = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese los minutos:  \r\n");
			time_var = minutos;
			break;
		case minutos:
			hora_config.min = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el nro de dia del mes:  \r\n");
			time_var = dia_mes;
			break;
		case dia_mes:
			hora_config.mday = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el nro de dia de la semana:  \r\n");
			time_var = dia_semana;
			break;
		case dia_semana:
			hora_config.wday = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el nro de mes:  \r\n");
			time_var = mes;
			break;
		case mes:
			hora_config.month = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el año:  \r\n");
			time_var = anio;
			break;
		case anio:
			hora_config.year = dat;
			UartSendString(SERIAL_PORT_PC, "Son las:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "  del:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.mday, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.month, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.year, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			time_var = hora;
			dat = 0;
			set_clock = FALSE;
			LedOff(LED_RGB_B);
			rtcWrite(&hora_config);
			clock_ready=TRUE;
			break;
		}

	}

	/******* RECUPERO COMANDOS **********/
	switch(dat){

	case 'd':
		UartSendString(SERIAL_PORT_PC, "Ingrese la hora actual:\r\n");
		set_clock= TRUE;
		LedOn(LED_RGB_B);
		break;
	case 'p':
		ReturnBlindStatus();
		break;
	case 'h':

		UartSendString(SERIAL_PORT_PC, "Humedad:\r\n ");
		UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureHumidity(), 10));
		UartSendString(SERIAL_PORT_PC, " % \r\n");
		break;

	case 't':
		UartSendString(SERIAL_PORT_PC, "Temperatura:\r\n ");
		UartSendString(SERIAL_PORT_PC, UartItoa(Si7007MeasureTemperature(), 10));
		UartSendString(SERIAL_PORT_PC, " °C \r\n");
		break;

	case 'i':


		if(MeasureIlumination()==HIGH)
			UartSendString(SERIAL_PORT_PC, "La iluminacion es buena \r\n");
		if(MeasureIlumination()==LOW)
			UartSendString(SERIAL_PORT_PC, "La iluminacion es baja \r\n ");
		break;

	case 'm':
		UartSendString(SERIAL_PORT_PC, "/---------------MENU------------/\r\n");

		UartSendString(SERIAL_PORT_PC, "ingrese 'h' para recuperar % de humedad:\r\n");
		UartSendString(SERIAL_PORT_PC, "ingrese 't' para recuperar la temperatura:\r\n");
		UartSendString(SERIAL_PORT_PC, "ingrese 'i' para recuperar iluminacion:\r\n");

		/** etc...*/
		UartSendString(SERIAL_PORT_PC, "Ingrese 'p' para recuperar el estado de las persianas:\r\n");

		/** etc...*/
		UartSendString(SERIAL_PORT_PC, "Ingrese 'd' para configurar la hora:\r\n");
		UartSendString(SERIAL_PORT_PC, "Presione TEC 4 para cambiar entre control manual y automatico:\r\n");



		break;
	}
}

void SisInit(){

	Si7007Init(&HT_config);

	SwitchesInit();

	SwitchActivInt(SWITCH_1,ManualRaiseBlinds);
	SwitchActivInt(SWITCH_2,ManualLowerBlinds);
	SwitchActivInt(SWITCH_4,ChangeControlMethod);

	GPIOInit(GPIO_1, GPIO_INPUT);
	GPIOInit(GPIO_3, GPIO_INPUT);   		//estos van a ver el estado de los finales de carrera del motor
	GPIOInit(GPIO_LCD_1,GPIO_INPUT);		//este es para la salida del sensor de luz


	manual_control=TRUE;

	PWMInit(ctout, 2, 20000);

	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);
	rtcConfig(&hora_config);



	if(GPIORead(GPIO_1))
		LowerBlinds();

	UartSendString(SERIAL_PORT_PC, "Ingrese 'm' para ver el menu:\r\n");
}

int main(void){
	SisInit();

	while(1){


	}
	return 0;

}

/*==================[end of file]============================================*/

