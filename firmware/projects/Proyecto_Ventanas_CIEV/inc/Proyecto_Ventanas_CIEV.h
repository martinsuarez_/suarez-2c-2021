/*! @mainpage Proyecto_Ventanas_CIEV
 *
 * \section genDesc General Description
 *
 * This application controls blinds manually and automatically.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Si7007	    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	SELECT	 	| 	 GPIO5	    |
 * | 	VDD	 		| 	 VDDA		|
 * | 	PWM2 	 	| 	 CH1    	|
 * | 	PWM1	  	| 	 CH2		|
 * |	GND			|	 GNDA 		|
 *
 * | 	Motor 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 VDD  	    |  	  3,3 V	    |
 * | 	 PWM1	    | 	  TFIL2		|
 * | 	 PWM2	    | 	  TFIL1   	|
 * |   	 DO1        |  	  GPIO3		|
 * |   	 DO2  	    |  	  GPIO1		|
 * |   	 GND 	    |  	  GND		|
 *
 * | Light sensor	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC	 		| 	 3,3 V	    |
 * | 	GND	 		| 	 GND		|
 * | 	DO 	 		| 	 LCD1		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/10/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Martin Suarez
 *
 */

#ifndef _PROYECTO_VENTANAS_CIEV_H
#define _PROYECTO_VENTANAS_CIEV_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO_VENTANAS_CIEV_H */

