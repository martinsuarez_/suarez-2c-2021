/*! @mainpage Ultrasonido 2
 *
 * \section genDesc General Description
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |   Display  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 D1  	    |  	  LCD1	    |
 * | 	 D2	        | 	  LCD2		|
 * | 	 D3         | 	  LCD3   	|
 * |   	 D4         |  	  LCD4 		|
 * |   	 SEL_0      |  	  GPIO1		|
 * |   	 SEL_1      |  	  GPIO4		|
 * |     SEL_2      |  	  GPIO5		|
 * | 	 +5V 	 	| 	  +5V    	|
 * | 	 GND	   	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * |22/09/2021  |	Added Serial port communication               |
 *
 * @author Martin Suarez
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Medidor_de_distancia_por_ultrasonido_con_interrupciones.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "led.h"
#include "stdio.h"
#include "switch.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
uint8_t distancia=0;
uint8_t tecla=0;
uint8_t activo=0;
uint8_t hold=0;
gpio_t pins_config[7]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};

/*==================[internal functions declaration]=========================*/

void Medir();
void MedirSerie();

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,1000,Medir};
serial_config UART_USB = {SERIAL_PORT_PC,115200,MedirSerie};

/*==================[external functions definition]==========================*/
void Medir(){

	if(activo)	{

		distancia=HcSr04ReadDistanceCentimeters();

		if(!hold){
			ITSE0803DisplayValue(distancia);
			UartSendString(SERIAL_PORT_PC, UartItoa(distancia, 10));
			UartSendString(SERIAL_PORT_PC, " cm\r\n");


			if (distancia<10) {
				LedOn(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			if (distancia>=10 & distancia<20) {LedOn(LED_RGB_B);
			LedOn(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			}
			if (distancia>=20 & distancia<30) {LedOn(LED_RGB_B);
			LedOn(LED_1);
			LedOn(LED_2);
			LedOff(LED_3);
			}
			if (distancia>30){
				LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);
			}
		}
	}
	else {
		ITSE0803DisplayValue(0);
		LedOff(LED_RGB_B);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	DelaySec(1);
	distancia=0;
}
void MedirSerie(){

	uint8_t dato_por_teclado;



	UartReadByte(SERIAL_PORT_PC, &dato_por_teclado);

	switch (dato_por_teclado){
	case 'O':
		Tecla1();
		break;
	case 'H':
		Tecla2();
	}

}

void Tecla1(void){
	UartSendString(SERIAL_PORT_PC, "Se cambia el estado de activo\n\r");
	activo=!activo;
}
void Tecla2(void){
	UartSendString(SERIAL_PORT_PC, "Se cambia el estado hold\n\r");
	hold=!hold;
}
void SisInit(void){
	SystemClockInit();
	ITSE0803Init(pins_config);
	LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);


	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);

	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);
}

int main(void){
	//	SystemClockInit();
	//	ITSE0803Init(pins_config);
	//	LedsInit();
	//	SwitchesInit();
	//	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	//	SwitchActivInt(SWITCH_1,tecla_1);
	//	SwitchActivInt(SWITCH_2,tecla_2);
	//
	//	TimerInit(&my_timer);
	//	TimerStart(TIMER_A);

	SisInit();
	while(1){

	}
	return 0;
}

/*==================[end of file]============================================*/

