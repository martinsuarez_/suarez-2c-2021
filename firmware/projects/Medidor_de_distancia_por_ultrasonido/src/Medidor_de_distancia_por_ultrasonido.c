/*! @mainpage Ultrasonido 1
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
* |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |   Display  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 D1  	    |  	  LCD1	    |
 * | 	 D2	        | 	  LCD2		|
 * | 	 D3         | 	  LCD3   	|
 * |   	 D4         |  	  LCD4 		|
 * |   	 SEL_0      |  	  GPIO1		|
 * |   	 SEL_1      |  	  GPIO4		|
 * |     SEL_2      |  	  GPIO5		|
 * | 	 +5V 	 	| 	  +5V    	|
 * | 	 GND	   	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Martin Suarez
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Medidor_de_distancia_por_ultrasonido.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "led.h"
#include "stdio.h"
#include "switch.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
uint8_t distancia=0;
uint8_t tecla=0;
uint8_t activo=0;
uint8_t hold=0;
gpio_t pins_config[7]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	ITSE0803Init(pins_config);
	LedsInit();
	SwitchesInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	while(1){

		tecla=SwitchesRead();

		switch(tecla){
		case SWITCH_1:
			activo=!activo;
			break;
		case SWITCH_2:
			hold=!hold;
			break;
		}
		if(activo)	{
			distancia=HcSr04ReadDistanceCentimeters();

			if(!hold){
				ITSE0803DisplayValue(distancia);
				if (distancia<10) {
					LedOn(LED_RGB_B);
					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				if (distancia>=10 & distancia<20) {LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				}
				if (distancia>=20 & distancia<30) {LedOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOff(LED_3);
				}
				if (distancia>30){
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOn(LED_3);
				}
			}
		}
		else {
			ITSE0803DisplayValue(0);
			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		}
		DelaySec(1);
		distancia=0;
	}
	return 0;
}

/*==================[end of file]============================================*/

